import {Handler, Request} from "../Handler";
import {CoreSocketService} from "../tower/CoreSocketService";
import {Record, String, Static, Partial, Union, Undefined, Number} from "runtypes";
import {IPackage} from "../tower/packages";
import {RequestHandlingError} from "../RequestHandlingError";

const RequestRequestType = Record({
    className: String,
    responseClassName: Union(Undefined, String),
    timeout: Union(Undefined, Number),
    data: Partial({
        id: Union(Undefined, String)
    })
});

type RequestRequest = Static<typeof RequestRequestType>;

interface RequestResponse {
    data: IPackage;
}

export class RequestHandler extends Handler<RequestRequest, RequestResponse> {

    constructor(private readonly coreSocketService: CoreSocketService) {
        super(RequestRequestType);
    }

    protected async handle(request: Request<RequestRequest>): Promise<RequestResponse> {
        const data = <IPackage>request.data.data;
        try {
            return {
                data: await this.coreSocketService.execute(request.data.className, data, request.data.timeout,
                    request.data.responseClassName)
            };
        } catch (e) {
            console.error(e);
            if (e instanceof Error) {
                throw new RequestHandlingError(e.message, 500);
            }
            throw new RequestHandlingError("Internal server error", 500);
        }
    }
}