import {Handler, Request} from "../Handler";
import {CoreSocketService} from "../tower/CoreSocketService";
import {Record, String, Unknown, Static, Partial, Union, Undefined} from "runtypes";
import {IPackage} from "../tower/packages";
import {RequestHandlingError} from "../RequestHandlingError";

const SendRequestType = Record({
    className: String,
    data: Partial({
        id: Union(Undefined, String)
    })
});

type SendRequest = Static<typeof SendRequestType>;

export class SendHandler extends Handler<SendRequest, unknown> {

    constructor(private readonly coreSocketService: CoreSocketService) {
        super(SendRequestType);
    }

    protected async handle(request: Request<SendRequest>): Promise<unknown> {
        const data = <IPackage>request.data.data;
        try {
            this.coreSocketService.emit(request.data.className, data);
        } catch (e) {
            if (e instanceof Error) {
                throw new RequestHandlingError(e.message, 500);
            }
            throw new RequestHandlingError("Internal server error", 500);
        }
        return {};
    }
}