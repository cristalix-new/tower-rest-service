import Bluebird from "bluebird";
// eslint-disable-next-line @typescript-eslint/no-explicit-any
global.Promise = <any>Bluebird;

import dotenv from "dotenv";

dotenv.config();

import {WebServer} from "./WebServer";
import {CoreSocketService} from "./tower/CoreSocketService";
import {SendHandler} from "./handlers/SendHandler";
import {RequestHandler} from "./handlers/RequestHandler";

const coreSocketService = new CoreSocketService({
    realmType: "REST",
    realmId: 0,
    towerLogin: process.env.TOWER_LOGIN ?? "tower-login",
    towerPassword: process.env.TOWER_PASSWORD ?? "tower-password",
    towerUrl: process.env.TOWER_URL ?? "tower-url"
});

const server = new WebServer();
server.addHandler("/api/send", "POST", new SendHandler(coreSocketService));
server.addHandler("/api/request", "POST", new RequestHandler(coreSocketService));

(async () => {
    coreSocketService.open();
    server.start(parseInt(<string>process.env.SERVER_PORT) || 8080);
})().catch(error => {
    console.error(error);
    process.exit(1);
});
