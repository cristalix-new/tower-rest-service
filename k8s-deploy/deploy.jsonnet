local is_production = std.extVar('IS_PRODUCTION') == 'true';
local namespace = std.extVar('KUBE_NAMESPACE');
local base_domain = std.extVar('BASE_DOMAIN');

[
  {
    kind: 'Secret',
    apiVersion: 'v1',
    metadata: {
      name: 'tower-rest-service-pull-credentials',
    },
    data: {
      '.dockerconfigjson': std.base64(std.manifestJson({
        auths: {
          [std.extVar('CI_REGISTRY')]: {
            auth: std.base64(std.extVar('CI_DEPLOY_USER') + ':' + std.extVar('CI_DEPLOY_PASSWORD'))
          },
        },
      })),
    },
    type: 'kubernetes.io/dockerconfigjson',
  },

  {
    kind: 'Secret',
    apiVersion: 'v1',
    metadata: {
      name: 'tower-credentials',
    },
    data: {
      login: std.base64(std.extVar('TOWER_LOGIN')),
      password: std.base64(std.extVar('TOWER_PASSWORD')),
    },
  },

  {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: 'tower-rest-service',
      labels: {
        app: 'tower-rest-service'
      },
      annotations: {
        'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
        'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: 'tower-rest-service',
        },
      },
      template: {
        metadata: {
          labels: {
            app: 'tower-rest-service',
          },
          annotations: {
            'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
            'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
          }
        },
        spec: {
          containers: [
            {
              image: '%s:%s' % [std.extVar('CI_REGISTRY_IMAGE'), std.extVar('CI_COMMIT_SHA')],
              name: 'tower-rest-service',
              env: [
                {
                  name: 'TOWER_LOGIN',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'tower-credentials',
                      key: 'login'
                    },
                  },
                },
                {
                  name: 'TOWER_PASSWORD',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'tower-credentials',
                      key: 'password'
                    },
                  },
                },
              ],
              ports: [
                {
                  containerPort: 8080,
                  protocol: 'TCP',
                },
              ],
            },
          ],
          imagePullSecrets: [
            {
              name: 'tower-rest-service-pull-credentials'
            },
          ],
        },
      },
    },
  },

  {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: 'tower-rest-service',
    },
    spec: {
      selector: {
        app: 'tower-rest-service',
      },
      ports: [
        {
          name: 'http',
          protocol: 'TCP',
          port: 80,
          targetPort: 8080,
        }
      ],
    },
  },
]